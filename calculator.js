// console.log("welcome to genjustu 😵 ");

var myVar = 321; /// the var variabe is belong to window object.
let myLet = 321; // and other variable are have block and local scope.
const myConst = 321;

/*********************************************************************************************/

myVariable = "at global"; // this are global variables this also belong to window object.

// console.log("myVariable",myVariable);// at global
{
  myVariable = "innerBlock";
  // console.log("myVariable",myVariable); //innerBlock,   the output is "innerBlock". becouse this are global variable that can cgange by in any scope.
}
// console.log("myVariable",myVariable);// innerBlock,  becouse now it have change by in global way.

/// var variable with block level example.
var myVar2 = "secondVar";

// console.log("myVar2",myVar2);//  secondVar
{
  var myVar2 = "321";
  // console.log("myVar2",myVar2); // "321"
  {
    var myVar2 = "654";
    // console.log("myVar2",myVar2); //  "654"
  }
  {
    var myVar2 = "987";
    // console.log("myVar2",myVar2);//  "987"
  }
  // console.log("myVar2",myVar2); //  "987"
}
// console.log("myVar2",myVar2);

(() => {
  _gobalVariable = "this is globale variable!";
})();
// console.log("_gobalVariable:=-   ",_gobalVariable);

// "987" this heppening becouse of var is having functional scope but note the block scope so becouse of  that every time var is declaring in global scope so that after each initilizaion it takes that new value.

// IIFE without parameter.

// (()=>{
//         // console.log("this IIFE function is calling by without parameters.")
// })();

/// an calculator which belong inside window object. and we can also store that calculator Object in defferent variable and then we can use then in node or we can use that all functionality with that variable.
// IIFE with passing parameters.
const myCalculator = (function (_global) {
  let calculatorObj = {};
  const add = function (a1, a2) {
    return a1 + a2;
  };
  const mult = function (a1, a2) {
    return a1 * a2;
  };
  const subtr = function (a1, a2) {
    return a1 - a2;
  };
  const divide = function (a1, a2) {
    return a1 / a2;
  };
  calculatorObj.addition = add;
  calculatorObj.multiplication = mult;
  calculatorObj.subtraction = subtr;
  calculatorObj.divide = divide;
  Object.freeze(calculatorObj);
  // console.log("calculatorObj",calculatorObj);
  _global.calculator = calculatorObj;
  return _global;
})(window || document || {});

// console.log("add",calculator.addition(5,6));
// console.log("mult",calculator.multiplication(5,6))
// console.log("subt",calculator.subtraction(5,6))
// console.log("divide",calculator.divide(5,6))

// an simple IIFE calling with paramiter.
const myIIFE = (function (something) {
  return (function (go) {
    // console.log("go",go);
  })(something);
})("hardik");

/// calculator with the example of closuers.
function calculater(operand) {
  return function (input1, input2) {
    if (isNaN(parseInt(input1)) || isNaN(parseInt(input2)))
      return "give valid number.";
    const inp1 = parseInt(input1);
    const inp2 = parseInt(input2);
    const oparation = {
      "+": (a, b) => a + b,
      "-": (a, b) => a - b,
      "*": (a, b) => a * b,
      "/": (a, b) => a / b,
    };
    return oparation[operand](inp1, inp2);
  };
}

// const add = calculater("+");
// const mult = calculater("*");
// const sub = calculater("-");
// const devide = calculater("/");
// console.log(add(5,8));
// console.log(mult(5,8));
// console.log(sub(5,8));
// console.log(devide("5",8));
// console.log(devide("+",8));

///// example with clouser chaning or currying.

function myOuterFunc(value1) {
  return function innerFunction(value2) {
    return value1 + value2;
  };
}

const add5 = myOuterFunc(5);
const add10 = myOuterFunc(10);
// console.log("add5=-  ",add5(10));
// console.log("add5=-  ",add10(10));

/// an infinite  currying example.

function addNumbers(value1) {
  return function (value2) {
    if (value2) {
      const sum = value1 + value2;
      return addNumbers(sum);
    }
    return value1;
  };
}

// console.log("addNumbers",addNumbers(1)(2)(5)(8)(4)(5)());

// Callback functions      *****************************************************************

// type of callback ==
//         synconys
//         asyncronys

// syncrons CallBack function

// function download(url,procces){
//     // this is syncrons callback function method
//     // do some calculation or or fetch data.
//     // then call your callback function.

//     console.log("downloading from url :=-  ",url);
//     procces(url);
// }

// function proccesData(data){
//     console.log("your procced data is from :=-  ",data);
// }

// let url = "https://www.google.com";

// download(url,proccesData);

function downloadAsyncronsly(url, procces) {
  // this is syncrons callback function method
  // do some calculation or or fetch data.
  // then call your callback function.
  console.log("downloading from url :=-  ", url);
  setTimeout(() => {
    procces(url);
  }, 5000);
}

// function proccesData(data){
//     console.log("your procced data is from :=-  ",data);
// }

// let url1 = "https://www.google.com";

//   asyncrons callback function with taking real time data from fake shop API!!!!
// downloadAsyncronsly(url1,proccesData);
async function downloadAsyncronsly(url, procces) {
  // this is syncrons callback function method
  // do some calculation or or fetch data.
  // then call your callback function.

  // const data = await fetch(url).then(a=>a.json());
  // const data = await
  fetch(url)
    .then((a) => a.json())
    .then((data) => {
      procces(data);
    })
    .catch((error) => {
      console.log("error ==", error);
    });
  // setTimeout(()=>{
  // procces(data);
  // },5000)
}

function proccesData(data) {
  // console.log("your procced data is from :=-  ",data);
  data.forEach((element) => {
    displayData(element);
  });
}

// const baseURL = "http://api.fakeshop-api.com/products/getAllProducts";
// const baseURL = "https://www.fakeshop-api.com/products/getAllProducts";
const baseURL = "https://fakestoreapi.com/products";

// downloadAsyncronsly(baseURL, proccesData);

function displayData(arrayData) {
  const card = document.createElement("div");
  card.className = "card";

  const image = document.createElement("div");
  image.className = "image";
  card.appendChild(image);
  const img = document.createElement("img");
  img.src = arrayData.image;
  image.appendChild(img);

  const content = document.createElement("div");
  content.className = "content";
  card.appendChild(content);

  const title = document.createElement("div");
  title.className = "title";
  title.textContent = arrayData.title;
  content.appendChild(title);

  const description = document.createElement("div");
  description.className = "description";
  description.textContent = arrayData.description;
  content.appendChild(description);

  const price = document.createElement("div");
  price.className = "price";
  price.textContent = "$  " + arrayData.price;
  content.appendChild(price);

  const ratings = document.createElement("div");
  ratings.className = "ratings";
  content.appendChild(ratings);

  const rate = document.createElement("span");
  rate.className = "rate";
  rate.textContent = `rating:- ${arrayData.rating.rate}`;
  ratings.appendChild(rate);

  const rateCount = document.createElement("span");
  rateCount.className = "rateCount";
  rateCount.textContent = `rating count:- ${arrayData.rating.count}`;
  ratings.appendChild(rateCount);

  const container = document.querySelector(".container");
  container.appendChild(card);
}

//  const myOBJ =
//     {
//     category : "men's clothing",
//     description : "Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday",
//      id : 1,
//     image :"https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
//     price : 109.95,
//     rating :{rate: 3.9, count: 120},
//     title : "Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops"
//     }

// Pyramid of Doom ****************************************************************************************************

/////////////  callback hell*******************************************88888
function download(url, procces) {
  console.log("downloading from url :=-  ", url);
  setTimeout(() => {
    procces(url);
  }, 5000);
}

let url1 = "url 111111111111111";
let url2 = "url 222222222222222";
let url3 = "url 333333333333333";

// download(url1, function (value) {
//   console.log("value 1==", value);

//   download(url2, function (value) {
//     console.log("value 2==", value);

//     download(url3, function (value) {
//       console.log("value 3==", value);
//     });
//   });
// });

// Take an array of numbers, it should only print even numbers on the page  ***********************

const numArray = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
];

function printEvenNumToDocument(array, callback) {
  for (let i = 0; i < array.length; i++) {
    if (array[i] % 2 == 0) {
      callback(array[i]);
    }
  }
}
function displayInDocument(numValue) {
  const numArrOutput = document.querySelector(".numArrOutput");
  const para = document.createElement("p");
  para.textContent = `=- ${numValue}`;
  numArrOutput.appendChild(para);
}
printEvenNumToDocument(numArray, displayInDocument);

// get min max value quiz form...............................

let maxNum;
let minNum;
function getMinMax() {
  const maxNumber = Math.floor(Math.random() * 50);
  const minNumber = maxNumber - 10;
  if (maxNumber < 11) {
    return getMinMax();
  }
  const inp_labal = document.querySelector(".inp_labal");
  inp_labal.textContent = `find Number between min-max (${minNumber} - ${maxNumber})`;
  // return { minNum, maxNum };
  maxNum = maxNumber;
  minNum = minNumber;
}

getMinMax();
// const { minNum, maxNum } = getMinMax();
document.querySelector(".quiz_form").addEventListener("submit", function (e) {
  e.preventDefault();
  const numberValue = document.getElementById("number").value;
  const err_msg = document.querySelector(".err_msg");

  console.log("min max", minNum, maxNum);
  try {
    if (numberValue == "") {
      err_msg.style.color = "red";
      throw "the number value is empty.";
    }
    if (isNaN(numberValue)) {
      err_msg.style.color = "red";
      throw "the given number is not a number value.";
    }
    if (numberValue < minNum) {
      err_msg.style.color = "red";
      throw "the given number is too low.";
    }
    if (numberValue > maxNum) {
      err_msg.style.color = "red";
      throw "the given number is too high.";
    }
    if (maxNum > numberValue < minNum) {
      err_msg.style.color = "green";
      throw `its between min-max ${minNum} - ${maxNum} 🎊`;
    }
  } catch (error) {
    err_msg.style.display = "block";
    err_msg.textContent = error;
    // err_msg.style.color = "red";
  } finally {
    setTimeout(() => {
      // err_msg.style.display = "none";
      err_msg.textContent = "";
    }, 3000);
  }
});

// Array.prototype.myForEach = function (cb, thisObj) {
//   const thisObjFunction = cb.bind(thisObj);
//   for (let i = 0; i < this.length; i++) {
//     thisObjFunction(this[i], i, this);
//   }
// };

// try {
//   throw new EvalError("EvalError has occurred"); throwing error with creating new object.
// } catch (e) {
//   console.log(e instanceof EvalError); // true
//   console.log(e.message); // EvalError has occurred
//   console.log(e.name); // EvalError
// }

// var el_down = document.getElementById("GFG_DOWN");
// function Geeks() {
//   try {
//     (3.54).toFixed(101); /// number must be in between 0 to 100. for toFixed(). number method.
//     el_down.innerHTML = "'Precision out of range'" + " error has not occurred";
//   } catch (e) {
//     console.log("error ====*********", e);
//     el_down.innerHTML = "'Precision out of range'" + " error has occurred";
//   }
// }

// var el_down = document.getElementById("GFG_DOWN");
// function Geeks() {
//   try {
//     (77.1234).toExponential(-8); //number passed in argument must be in between 0 to 100.
//     el_down.innerHTML = "'Precision out of range'" + " error has not occurred";
//   } catch (e) {
//     console.log("error ===**************", e);
//     el_down.innerHTML = "'Precision out of range'" + " error has occurred";
//   }
// }

// var el_down = document.getElementById("GFG_DOWN");
// function Geeks() {
//   try {
//     (5643.9).toPrecision(1); // number passed in argument must be in between 1 to 100;
//     el_down.innerHTML = "'Precision out of range'" + " error has not occurred";
//   } catch (e) {
//     console.log("error =========***************", e);
//     el_down.innerHTML = "'Precision out of range'" + " error has occurred";
//   }
// }

// var el_down = document.getElementById("GFG_DOWN");
// function GFG() {
//   "use strict";
//   var AR_GFG = { pro: "Val_1" };
//   return AR_GFG.prop_1;
// }
// function Geeks() {
//   try {
//     GFG();
//     console.log("error mesage = ", message); //ReferenceError:  message is not define.
//     el_down.innerHTML =
//       "'Reference to undefined property'" + " error has not occurred";
//   } catch (e) {
//     console.log("error from referance error ===*********************", e);
//     el_down.innerHTML =
//       "'Reference to undefined property'" + "error has occurred";
//   }
// }
// encodeURI("\uD810");

// decodeURIComponent("%E0%B4%A");
